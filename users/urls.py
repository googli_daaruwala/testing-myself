from django.urls import path
from .views import registeration, login, logout

urlpatterns = [
    path('register/', registeration, name='registeration'),
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),
]