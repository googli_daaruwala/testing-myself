from django.shortcuts import render, redirect
from django.contrib.auth.models import User, auth
from django.contrib import  messages
from django import forms
from django.http import HttpResponse

# Create your views here.

def registeration(request):
    if request.method == 'POST':
        name = request.POST['name']
        username = request.POST['username']
#        phone_number = request.POST['phone_number'] #couldn't add phone and name field to User model #refer forms.py for more
        email = request.POST['email']
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']

        if password == confirm_password:
            if User.objects.filter(username=username).exists():
                messages.warning(request,'username taken')
                return redirect('registeration')
            elif User.objects.filter(email=email).exists():
                messages.warning(request,'email taken')
                return redirect('registeration')
            else:    
                user = User.objects.create_user(first_name=name,  username=username, email=email, password=password)
                user.save()
                return redirect('login')
        else:
            messages.warning(request,'Passwords didn\'t match' )
    else:
        return render(request,'users/registeration.html')

def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request,user)
            return redirect('/')
        else:
            messages.warning(request,'wrong Username or Password')

    else:
        return render(request,'users/login.html')

def logout(request):
    auth.logout(request)
    return redirect('login')